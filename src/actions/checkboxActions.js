const SET_CHECKED_STATE = "SET_CHECKED_STATE"

export function setChecked(index) {
    return {
        type: SET_CHECKED_STATE,
        payload: index
    }
}
