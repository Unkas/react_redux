import React, { Component } from 'react'
import logo from '../logo.svg'
import '../App.css'
import List from '../components/List'


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          {this.props.state.boxes.length} checkboxes
        </p>
        <List boxes_count={this.props.state.length} props={this.props}/>
      </div>
    )
  }
}

export default App