import React, { Component } from 'react'
var classNames = require('classnames')

class Checkbox extends Component {

  handleClick()  {
    this.props.props.setChecked(this.props.id)
  }

  render() {
    var checkboxClass = classNames({
      'custom-checkbox': true,
      'custom-checkbox custom-checkbox_checked': this.props.props.state.boxes[this.props.id-1].checked
    })
    return (
      <li className={checkboxClass} onClick={this.handleClick.bind(this)}> {/*::this.handleClick*/}
      </li>
    )
  }
}

export default Checkbox
