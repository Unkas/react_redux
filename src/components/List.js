import React from 'react'
import Checkbox from './Checkbox.js'

const List = (props) => {
  return <ul>
    {props.props.state.boxes.map((checkbox, index) =>
      <Checkbox key={checkbox.key} id={checkbox.key} props={props.props} />
    )}
  </ul>
}

export default List