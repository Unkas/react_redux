import {connect} from "react-redux"
import {setChecked } from '../actions/checkboxActions'
import App from '../components/App'

const mapStateToProps = (state) => {
  return { state: state }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setChecked: (index) => {
          dispatch(setChecked(index))
      }        
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)