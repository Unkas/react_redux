const BOXES_COUNT = 999
export { BOXES_COUNT }

const INITIAL_STATE = {boxes: [], anotherState: []}
for (var i = 1; i < BOXES_COUNT + 1; i++) {
  INITIAL_STATE.boxes.push({"key": i, "checked": false})
}
export { INITIAL_STATE }

const checkboxReducer = (state={}, action) => {
  if (action.type === "SET_CHECKED_STATE") {
    var copy = Object.assign({}, state)
    copy.boxes[action.payload-1].checked = !copy.boxes[action.payload-1].checked
    state = copy
  }
  return state
}
export default checkboxReducer