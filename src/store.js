import checkboxReducer from './reducers/checkboxReducer'
import { INITIAL_STATE } from './reducers/checkboxReducer'
import {createStore, applyMiddleware} from 'redux'
import logger from 'redux-logger'

let state = INITIAL_STATE

export default createStore(checkboxReducer, state, applyMiddleware(logger()) )
